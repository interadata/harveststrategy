---
title: "Introduction to harvestStrategy"
author: "Dr Stephen Lane, interadata"
date: "`r Sys.Date()`"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Introduction to harvestStrategy}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

The **harvestStrategy** package contains functions that enable the production of a Harvest Strategy for the Victorian Fisheries Authority. At present, this package has been developed with the particular application of an abalone harvest strategy. The package is easily extensible to other fisheries however.

## Calculation of indicators

The `harvestStrategy` package comes with functions to calculate indicators directly. Here we demonstrate how to use these functions on the packaged data set.

```{r cpue}
library(dplyr)
library(purrr)
library(tidyr)
library(harvestStrategy)
library(ggplot2)
theme_set(theme_bw(14))
data(cpue)
glimpse(cpue)

```

The `cpue` dataset contained in the `harvestStrategy` package has data on catch per unit effort (CPUE) for five spatial management units (SMU) over ten years. One indicator of interest in fisheries management is the proportional increase/decrease in the CPUE from year to year. We can calculate this using the `ratioIndicator` function:

```{r ratio-indicator}
cpue_indicate <- cpue %>%
    group_by(SMU) %>%
    mutate(CPUE_ratio = ratioIndicator(CPUE))
glimpse(cpue_indicate)

```

We can of course plot these simply using the `ggplot2` package:

```{r plot-ratio}
pl_ratio <- ggplot(cpue_indicate,
                   aes(x = Year, y = CPUE_ratio, group = SMU, colour = SMU)) +
    geom_line() +
    theme(legend.position = "bottom")
pl_ratio

```

There are two more inbuilt functions to calculate indicators in the `harvestStrategy` package: `movingIndicator` and `distanceToTarget`.

`movingIndicator` calculates an indicator on a moving window with user-specified `size`; the indicator function is likewise user-specified. `harvestStrategy` comes with one inbuilt function for use with the `movingIndicator` function: `movingGradient`. `movingGradient` calculates the ordinary least squares slope parameter using `size` sequential data points. This is demonstrated below where we calculate the four-year gradient indicator for CPUE:

```{r moving-indicator}
cpue_indicate <- cpue_indicate %>%
    group_by(SMU) %>%
    mutate(CPUE_gradient4 = movingIndicator(CPUE, movingGradient, 4))
glimpse(cpue_indicate)

```

Note that where calculations of indicators are not possible, `NA`s are returned.

`distanceToTarget` calculates the proportional distance of the input value, to the target value. For full details, see the function help (`?distanceToTarget`). To use this function, a named `list` or `vector` needs to be supplied (parameter `targetLookup`), where the names match the SMU names *exactly*. We create such a list in the example below; note however, that the `targetLookup` can exist in a csv file, and loaded in.

```{r distance-to-target}
target_lookup <- list(a = 65, b = 57, c = 77, d = 100, e = 110)
target_lookup
cpue_indicate <- cpue_indicate %>%
    group_by(SMU) %>%
    mutate(CPUE_distance = distanceToTarget(CPUE, SMU, target_lookup))
glimpse(cpue_indicate)

```

# Categorisation of indicators

The `harvestStrategy` package has functions to categorise the indicators, according to the relative change in indicators between successive years. The categorisation of indicators depending on how much they change may be particular to the abalone fishery that the package was developed for, but the functions are suitably flexible to apply to other fisheries.

For some categorisation functions, it will be of interest to first calculate the relative change of the indicator. The `relativeChange` function does this. Note that some indicators may involve negative numbers; thus, the relative change is defined as: $(y_{t} - y_{t-1}) / \text{abs}(y_{t-1})$, where $y_{t}$ is the indicator at time $t$, and $y_{t-1}$ is the indicator at time $t-1$. `relativeChange` is used in the following manner:

```{r relative-change}
cpue_indicate <- cpue_indicate %>%
    group_by(SMU) %>%
    mutate(CPUE_gradient4_rel = relativeChange(CPUE_gradient4))
glimpse(cpue_indicate)

```

## `categoriseIndicator`

The `categoriseIndicator` can be used to categorise an input into `Decreasing`, `Stable` or `Increasing` categories, given user-specified bounds.

In the example below, we categorise the CPUE gradient (four-year rolling slope) indicator (`CPUE_gradient4`) based on the bounds `c(-0.05, 0.05)` (see `?categoriseIndicator` for more details):

```{r categorise-cpue}
cpue_indicate <- cpue_indicate %>%
    mutate(CPUE_gradient4_cat = categoriseIndicator(
               CPUE_gradient4, c(-0.05, 0.05)
           ))
glimpse(cpue_indicate)

```

**Note**: by default if the relative change indicator has `NA` values, these will be given the category `Stable` by default. If you wish to change this, you can do so by setting the `default` parameter, e.g. `categoriseIndicator(value, bounds, default = "Increasing")`. This is useful when creating decision rules based on the categorisation of multiple indicators.

## `compoundCategorisation`

To create the final trend categorisation, indicator categories (such as those created by `categoriseIndicator` above) are combined via decision rules. The decision rules state how the indicators behave in combination. For example, if the primary category is `Increasing` and the secondary category is `Decreasing`, then the fishery is said to be in a `Stable` category.

For the fishery of the subject of this package, there are two compound categorisations that occur: the first combines the primary and secondary categories as just described into a *compound_category*; the *compound_category* is then compounded with the tertiary indicator into a final categorisation. This is what happens in the code below. First, the extra categories are created from different indicators (`CPUE_ratio` and `CPUE_distance`), then they are combined using some inbuilt decision rules:

```{r compound-categorisation}
## Create further indicator categories
cpue_indicate <- cpue_indicate %>%
    mutate(CPUE_ratio_rel = relativeChange(CPUE_ratio),
           CPUE_ratio_cat = categoriseIndicator(
               CPUE_ratio_rel, c(-0.05, 0.05)
           ),
           CPUE_distance_rel = relativeChange(CPUE_distance),
           CPUE_distance_cat = categoriseIndicator(
               CPUE_distance_rel, c(-0.05, 0.05)
           ))
## Combine indicators into final category
data(prim_sec)
data(compound_tertiary)
cpue_indicate <- cpue_indicate %>%
    mutate(prim_sec_cat = purrr::map2_chr(
                                     CPUE_gradient4_cat,
                                     CPUE_ratio_cat,
                                     compoundCategorisation,
                                     decisionRule = prim_sec),
           final_cat = purrr::map2_chr(
                                  prim_sec_cat,
                                  CPUE_distance_cat,
                                  compoundCategorisation,
                                  decisionRule = compound_tertiary,
                                  primary_category = "compound_category",
                                  secondary_category = "tertiary_category"
                              )
           )
glimpse(cpue_indicate)

```

There are two things to note in the above code: first, the second application of `compoundCategorisation` changes the names of the categories, to match with compound categorisation produced in the first step. Secondly, each application uses some inbuilt decision rules---these were useful for the applied example that this package was built for; if custom decision rules are required (and they should be for new fisheries), then look at the structure of those (`prim_sec` and `compound_tertiary`) for implementation.

# Catch control strategy

```{r split-cpue,echo=FALSE}
cpue_2016 <- cpue_indicate %>%
    filter(Year == 2016)
cpue_hist <- cpue_indicate %>%
    filter(Year < 2016)

```

With the categorisation complete, a catch control strategy may be applied. Here we will demonstrate some custom functions to choose the appropriate strategy for a forthcoming season. These functions have been designed with a specific application in mind, so will require tailoring for a new fishery.

## SMU reference limits

A catch control strategy will rely on the CPUE meeting certain targets/reference limits. The reference limits are designed to be responsive to the demands on the fishery, in order to keep the fishery viable.

The example fishery has the following reference ranges:

```{r cpue-ref}
data(cpue_ref)
cpue_ref

```

If CPUE is above the threshold, then a certain catch control strategy will be enacted. If it is below the limit, then a different strategy will be used. If CPUE is between the limit and threshold, then which strategy is used, depends on historical threshold results.

## Historical catch controls

As we have just described, historical threshold results are required to implement a new catch control strategy. For our example data, the historical threshold results look like the following:

```{r cpue-hist}
data(cpue_thresh)
cpue_thresh

```

## New catch control rule

Suppose that we have the following CPUE values for the 2016 season:

```{r cpue-2016}
cpue_2016 %>%
    select(SMU, Year, CPUE)

```

We can use the `catchControlStrategy` function to generate a new catch control rule for the 2017 season:

```{r new-ccs}
cpue_thresh <- cpue_thresh %>%
    filter(Year < 2016)
cpue_2016 <- cpue_2016 %>%
    mutate(
        ccr = purrr::pmap(list(SMU, Year, CPUE), catchControlStrategy,
                          hist_ccs = cpue_thresh,
                          cpue_ref = cpue_ref,
                          ccs_warning_nm = "thresh")
    ) %>%
    tidyr::unnest()
cpue_2016 %>%
    select(SMU, Year, CPUE, CCS_Warning, CCS)

```

The output above shows that all SMUs should have the catch control strategy CCR1 applied (which is determined by the fishery).

## Target catch ranges

As part of the package, we have included a data set that allows adjustment of target catches (tonnes), given a [catch control rule](#new-catch-control-rule), and a [final categorisation](#compoundcategorisation). This data set appears like the following:

```{r range-rules}
data(range_rules)
range_rules

```

What can be seen in this data, for example, is that if the catch control rule is CCR1, with final categorisation Increasing, the optimal catch target should be adjusted, with lower multiplier equal to 1.0 and upper multiplier equal to 1.15.

We include the function `catchControlRange` to perform this adjustment. `catchControlRange` requires the optimal target (tonnes) that was set for the SMU in the current season, the catch control strategy calculated from CPUE (and it's historical values, [see above](#historical-catch-controls)), and the final categorisation as calculated from the [indicators](#calculation-of-indicators).

For example, suppose that in 2016, the following optimal targets were set, with the [catch control strategy](#new-catch-control-rule) and [final categorisation](#compound-categorisation) as shown in their respective sections above:

```{r new-ot}
cpue_2016 <- cpue_2016 %>%
    left_join(
        .,
        data_frame(SMU = letters[1:5], OT = c(31.7, 16.5, 41.7, 21.0, 90.0)),
        by = "SMU"
    )


```

```{r optimal-targets}
cpue_2016 %>%
    select(SMU, Year, CCS, final_cat, OT)

```

The optimum targets for the 2017 season can be set from the ranges calculated by the `catchControlRange` function, and the range rules above like the following:

```{r new-ranges}
cpue_2016 <- cpue_2016 %>%
    mutate(
        ranges = purrr::pmap(list(OT, CCS, final_cat),
                             catchControlRange,
                             range_rules = range_rules)
    ) %>%
    tidyr::unnest()
cpue_2016 %>%
    select(SMU, Year, CCS, final_cat, OT, OT_Lower, OT_Upper)

```
