# Harvest Strategy

## Description

This package implements provides functions to implement a Harvest Strategy for a collection of fisheries, given historical catch data.

The package contains a series of functions which together produce a harvest strategy document.
