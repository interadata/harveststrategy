#' Assigns ranges for a new target catch
#'
#' \code{catchControlRanges} assigns a possible catch range (tonnes) for the SMU
#' for next year's catch. The new catch range is based on the optimal target set
#' for the SMU in the current year, and the catch control strategy assigned to
#' the SMU (by the \code{catchControlStrategy} function).
#'
#' \code{catchControlRanges} is meant to be run on a \emph{single} year's
#' worth of data, following the calculation of the \code{catchControlStrategy}.
#'
#' @param ot Numeric. Optimal target (tonnes) that was set for the current year.
#' @param ccs Character. Catch control strategy set for the SMU.
#' @param final_cat Character. Final categorisation of the indicators (as
#'     created by the \code{compoundCategorisation} function).
#' @param range_rules Data frame. Data frame containing the lower and upper
#'     multipliers to apply to \code{ot}. This data frame must contain the
#'     following (named exactly):
#' \describe{
#'   \item{CCS}{Catch control strategy (one of CCR1/CCR2)}
#'   \item{Final_Category}{Final category class (one of
#'     Decreasing/Stable/Increasing)}
#'   \item{Lower}{Lower multiplier (between 0 and 1)}
#'   \item{Upper}{Upper multiplier (greater than 1)}
#' }
#' 
#' @return A data frame containing optimal target range for the next season.
#' @export
catchControlRange <- function(ot, ccs, final_cat, range_rules) {
    all1 <- all(
        length(ot) == 1, length(final_cat) == 1, length(ccs) == 1
    )
    assertthat::assert_that(all1,
                            msg = "All inputs must consist of a single value."
                            )
    assertthat::assert_that(is.numeric(ot))
    assertthat::assert_that(ncol(range_rules) == 4)
    assertthat::assert_that(nrow(range_rules) == 6)
    OT_Lower <- OT_Upper <- Lower <- Upper <- NULL
    ot_rn <- dplyr::data_frame(ot, ccs, final_cat) %>%
        dplyr::left_join(., range_rules,
                         by = c("ccs" = "CCS",
                                "final_cat" = "Final_Category")
                         ) %>%
        dplyr::mutate(
                   `OT_Lower` = ot * Lower,
                   `OT_Upper` = ot * Upper
               ) %>%
        dplyr::select(OT_Lower, OT_Upper)
    ot_rn
}
